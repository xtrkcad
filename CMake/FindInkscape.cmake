
# Try to find the Inkscape command-line SVG rasterizer
# Once done this will define
#
# Inkscape_FOUND
# Inkscape_EXECUTABLE   Where to find Inkscape
# Inkscape_VERSION      The Inkscape version number
# Inkscape_EXPORT       Option to specify the destination file
# Inkscape_GUI          Option to disable the GUI if needed
#
# Module is from https://github.com/arx/ArxLibertatis

find_program(
	Inkscape_EXECUTABLE
	NAMES inkscape
    HINTS "C:/Program Files/Inkscape/bin" 
	DOC "Inkscape command-line SVG rasterizer"
)

execute_process(COMMAND ${Inkscape_EXECUTABLE} "--version" OUTPUT_VARIABLE _Inkscape_VERSION ERROR_QUIET)
STRING(REGEX MATCH "[1-9]\.[0-9\+\.[0-9]+" _Inkscape_VERSION ${_Inkscape_VERSION})

set(Inkscape_VERSION ${_Inkscape_VERSION} CACHE STRING "Inkscape Version")

execute_process(COMMAND ${Inkscape_EXECUTABLE} "--help" OUTPUT_VARIABLE _Inkscape_HELP ERROR_QUIET)

if(_Inkscape_HELP MATCHES "--without-gui")
	set(Inkscape_GUI "--without-gui" CACHE STRING "Inkscape option to disable the GUI if needed")
endif()

if(NOT DEFINED Inkscape_EXPORT)
	foreach(option IN ITEMS "--export-filename=" "--export-file=" "--export-png=")
		if(_Inkscape_HELP MATCHES "${option}")
			set(Inkscape_EXPORT "${option}" CACHE STRING "Inkscape option to specify the export filename")
			break()
		endif()
	endforeach()
	if(NOT DEFINED Inkscape_EXPORT)
		message(WARNING "Could not determine Inkscape export file option, assuming -o")
		set(Inkscape_EXPORT "-o " CACHE STRING "Inkscape option to specify the export filename")
	endif()
endif()

# handle the QUIETLY and REQUIRED arguments and set Inkscape_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Inkscape REQUIRED_VARS Inkscape_EXECUTABLE)