\# Notes:
\#
\# The "\u000" command is used to format the output. The command causes a blank line to appear between "bulleted" or "described" items.
\#
\cfg{html-local-head}{<meta name="AppleTitle" content="Edit Menu" />}

\H{editM}\i{Edit Menu}

\G{png.d/medit.png}

The \f{Edit Menu} shows commands affecting the \f{Main Canvas} (\K{mainW}).

\dd \i{Undo} - Reverses the last command.  Up to the last ten commands can be undone.  If there are no commands that can be undone the \f{Undo} item is disabled.  The \f{Undo} command can also be invoked by the \f{Undo} button (\K{cmdUndo}).

\u000

\dd \i{Redo} - Undoes the last Undo command.  The \f{Redo} command can also be invoked by the \f{Redo} button (\K{cmdUndo}).

\u000

\dd \i{Cut} - Moves the selected objects to the clipboard.

\u000

\dd \i{Copy} - Copies the selected objects to the clipboard.

\u000

\dd \i{Paste} - Copies the contents of the clipboard to the layout. Using the paste Menu command it will be placed at the center of the screen.
If the context menu paste is used or the short-cuts \e{Ctrl+v} or \e{Shift+Insert}, the parts will be placed at the cursor position.
Repeated Pastes without moving the cursor result in overlaid copies.
The newly pasted parts are in \f{Selected} state and can easily be moved into place. Any previously selected objects are unselected.

\u000

\dd \i{Delete} - Deletes the selected objects.

\u000

\dd \i{Move To Current Layer} - move all selected objects to the current Layer (\K{cmdLayer}).

\u000

\dd \i{Select All} - Selects all objects on the layout.

\u000

\dd \i{Select Current Layer} - selects all objects in the current Layer (\K{cmdLayer}).

\u000

\dd \i{Select By Index} - selects object by index number (\K{cmdSelectIndex}).

\u000

\dd \i{Deselect All} - unselects all objects on the layout. The same action can be achieved by pressing the ESC key.

\u000

\dd \i{Invert Selection} - selects all unselected objects and unselects all previously selected objects.

\u000

\dd \i{Select Stranded Track} - selects all track pieces that are not connected to any other objects. This helps cleaning up a drawing after many changes have been made.

\u000

\dd \i{Tunnel}\I{Track, Tunnel} - Hides or reveals selected tracks and adds a tunnel portal symbol where the track passes from one to the other.

\u000

\dd \i{Bridge}\I{Track, Bridge} - Adds or removes parapet symbols and a solid base to the track. The color is selectable (\K{cmdRgbcolor}).

\u000

\dd \i{Roadbed}\I{Track, Roadbed} - Adds or removes solid roadbed under the track. The color is selectable (\K{cmdRgbcolor}).

\u000

\dd \i{Ties/No Ties}\I{Track Ties}\I{Track No Ties} - Hides or reveals the ties on selected tracks.

\u000

\dd \i{Move To Front} - Moves selected object to foreground.

\u000

\dd \i{Move To Back} - Moves selected object to background.

\u000

\dd \i{Thin, Medium and Thick Tracks} \I{Thin Tracks}\I{Medium Tracks}
	\I{Thick Tracks}\I{Tracks, Thin}\I{Tracks, Medium}\I{Tracks, Thick}
	\I{Track Width}
	- set displayed rail width of selected tracks.

\u000

For more information on working with selected objects, see the Select (\K{cmdSelect}) command.

\rule

\S{cmdSelectIndex} \i{Select By Index}

An input window allows one or more index numbers to be entered. Multiple indexes are seperated by commas.

The object can not be selected if its Layer is frozen.

If the object is part of a module layer, all objects in the layer will be selected.

If the object is a tunnel object, and the Display Tunnel option is None, it will be set to Dashed. See Display options (\K{cmdDisplay}).

When an individual object is selected, its index number is displayed in the Info Message area, another source is the List Parts (\K{cmdEnum}) command with the \f{List Indexes} option.

\S{cmdAboveBelow} "Move To Front" and "Move to Back" \I{Move To Front} \I{Move To Back}

\G{png.d/btop_bottom.png}

The Move to Front and Move To Back commands move the selected objects (\K{cmdSelect}) to the top of the drawing list - the \f{Foreground} or to the bottom of the list - the \f{Background} making them more or less visible or prominent.

The effect is to make the objects move to be in front of all or behind all other objects.  This command is useful when dealing with filled shapes (\K{cmdDraw}).

Selected objects moved to the \f{Background} become unselected.

\rule


\S{addshortcutkeys} \i{Additional Shortcut Keys}

Additional Shortcut keys perform the following:

\dd \c{Ctrl+Z} - Undo

\u000

\dd \c{Ctrl+R} - Redo

\u000

\dd \c{Ctrl+C} - Copy

\u000

\dd \c{Ctrl+V} - Paste

\u000

\dd \c{Ctrl+X} - Cut

\u000

\dd \c{Esc} - Deselect All

\rule


\S{cmdDelete} \i{Delete}

\G{png.d/bdelete.png}

The \f{Delete} button is used to delete selected objects.  Remember, the \f{Undo} (\K{editM} or \K{cmdUndo}) command can be used to restore a delete object.

Deleting a track that is connected to an easement (\K{cmdEasement}) causes the easement to be deleted as well.

The \e{Delete} key invokes the Delete command.

\rule


\S{cmdTunnel} \i{Tunnel (Hide Tracks)}

\G{png.d/btunnel.png}

The \f{Tunnel} command is used to hide selected (\K{cmdSelect}) tracks (as in a tunnel).

Select the tracks you want to hide in a tunnel.  Selecting a hidden track 'reveals' it.

A tunnel portal is drawn where a hidden track connects with an 'unhidden' track.  You can split (\K{cmdSplitTrack}) a track to place a tunnel portal at a particular spot.

The \f{Draw Tunnels} Radio button group on the \f{Options>Display} dialog (\K{cmdDisplay}) controls whether hidden tracks are not drawn, drawn as dashed lines or drawn as normal lines.


\rule

\S{cmdTies} \i{Ties (Draw or Hide Ties)}

\G{png.d/bties.png}

The \f{Ties} command is used to hide Ties (or reshow them).  This may be useful for overlapping track, like dual gauge, or in areas like docks or yards where the track is covered.

Select the tracks you want to hide or show ties for.


\rule

\S{cmdBridge} \i{Bridge (Draw or Hide Abutments)}

\G{png.d/bbridge.png}

The \f{Bridge} command is used to add abutments to a track (as on an underbridge).

Select the tracks you want to be a bridge. Bridge abutments are drawn alongside the track and a solid layer is added under the ties. You can split (\K{cmdSplitTrack}) a track to shorten the length of the bridge, or to add an extra pier to the drawing.

If you perform this action on an existing bridge (or use Tunnel) the bridge is removed.


\rule

\S{cmdRoadbed} \i{Roadbed (Draw or Hide Base)}

\G{png.d/broadbed.png}

The \f{Roadbed} command is used to add a solid color under a track (as roadbed).

Select the tracks you want to have roadbed. A solid roadbed layer is drawn under the track.

If you perform this action on existing roadbed (or use Tunnel) the roadbed is removed. The bridge option takes precedence over roadbed.


\rule

\S{cmdUndo} Undo and Redo \I{Undo} \I{Redo}

\G{png.d/bgundo.png}

The \f{Undo} and \f{Redo} buttons invoke the same actions as the \f{Edit} menu items.  The name of last command will be displayed as the Balloon Help (\K{windowTerms}) for the \e{Undo} button.  The name of last undone command will be displayed as the Balloon Help for the \e{Redo} button.

\rule
