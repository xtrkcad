# XTrackCAD Version 5.3.0 GA Release Notes

Welcome to the XtrackCAD V5.3.0 GA Release!

V5.3.0 GA contains many bug fixes, new and updated parameter files, new features and enhancements.

Enjoy!

Dave, Martin and Adam, your volunteer developers.

PS The full change log is a file in the XtrkCAD download folder as CHANGELOG.md

The files written by XTrackCAD 5.2 are versioned to only be read by 5.2, but it can also read files from earlier versions. If you get into trouble, please reach out, we may be able to help - but always back-up.

We will fix important bugs you find in subsequent releases.

To report bugs, please use the SourceForge bugs reporting page https://sourceforge.net/p/xtrkcad-fork/bugs/

To discuss the Release, please use the user forum https://xtrackcad.groups.io/g/main/topics

# V5.3.0 GA Notes

This file contains installation instructions and up-to-date information regarding XTrackCad.

## Contents ##

* [About XTrackCad](#About)
* [License Information](#License)
* [Installation](#Installation)
* [Upgrading from earlier releases](#Upgrading)
* [Building](#Building)
* [Where to go for support](#Support)

<a name='About' />

## About XTrackCAD ##

XTrackCad is a powerful CAD program for designing Model Railroad layouts.

Some highlights:

* Easy to use.
* Supports any scale.
* Supplied with parameter libraries for many popular brands of turnouts, plus the capability to define your own.
* Automatic easement (spiral transition) curve calculation.
* Extensive help files and video-clip demonstration mode.

Availability:
XTrkCad Fork is a project for further development of the original XTrkCad
software. See the project homepage at <http://www.xtrackcad.org/> for news and current releases.


<a name='License' />

## License Information ##

**Copying:**

XTrackCad is copyrighted by Dave Bullis and Martin Fischer and licensed as
free software under the terms of the GNU General Public License v2 which
you can find in the file COPYING.


<a name='Installation' />

## Installation ##

Please see https://sourceforge.net/p/xtrkcad-fork/wiki/DownloadInstall/

**Note:** The Linux version has intrduced changes to make co-existance of Beta and GA releases easier.  Please see https://sourceforge.net/p/xtrkcad-fork/wiki/LinuxInstall/ for details.


<a name='Upgrading' />

## Upgrade Information ##

**Note:** The 5.2 version of XTrackCAD came with the several new features
like background images or extensions to notes. In order to support
this feature, an additional file format for layout files (.xtce) was added.
The old .xtc format is still supported for reading and writing. So
files from earlier versions of XTrackCAD can be read without problems.
Layouts that were saved in the new format cannot be read by older
versions of XTrackCAD.


<a name='Building' />

## Building ##

Please see <https://sourceforge.net/p/xtrkcad-fork/wiki/BuildNotes/>


<a name='Support' />

## Where to go for support ##

The following web addresses will be helpful for any questions or bug
reports

- The group mailing list <https://xtrackcad.groups.io/g/main>
- The group Wiki <http://xtrkcad.org>
- The official Sourceforge site <https://sourceforge.net/projects/xtrkcad-fork>

Thanks for your interest in XTrackCAD.

