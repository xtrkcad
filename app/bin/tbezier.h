/*
 * $Header: /home/dmarkle/xtrkcad-fork-cvs/xtrkcad/app/bin/tbezier.h,v 1.1 2005-12-07 15:47:36 rc-flyer Exp $
 */

/*  XTrkCad - Model Railroad CAD
 *  Copyright (C) 2005 Dave Bullis
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "common.h"
#include "track.h" //- drawLineType

typedef struct extraDataBezier_t {
	extraDataBase_t base;
	coOrd pos[4];
	DIST_T minCurveRadius;
	ANGLE_T a0, a1;
	DIST_T length;
	dynArr_t arcSegs;
	coOrd descriptionOff;
	LWIDTH_T segsLineWidth;
	wDrawColor segsColor;
	drawLineType_e lineType;
} extraDataBezier_t;


void SetBezierData( track_p p, coOrd pos[4], wDrawColor color,
                    LWIDTH_T lineWidth );
track_p NewBezierTrack(coOrd[4], trkSeg_p, int );
track_p NewBezierLine(coOrd[4], trkSeg_p, int, wDrawColor, DIST_T);
void FixUpBezier(coOrd[4], struct extraDataBezier_t*, BOOL_T);
void FixUpBezierSeg(coOrd[4], trkSeg_p, BOOL_T);
void FixUpBezierSegs(trkSeg_p p,int segCnt);
BOOL_T GetBezierSegmentFromTrack(track_p, trkSeg_p);
BOOL_T GetTracksFromBezierTrack(track_p trk, track_p newTracks[2]);
BOOL_T GetTracksFromBezierSegment(trkSeg_p bezSeg, track_p newTracks[2],
                                  track_p old);
void SetBezierLineType( track_p trk, int width );
BOOL_T GetBezierMiddle( track_p, coOrd * );

void MoveBezier( track_p trk, coOrd orig );
void RotateBezier( track_p trk, coOrd orig, ANGLE_T angle );

DIST_T 	BezierDescriptionDistance(coOrd pos,track_p trk, coOrd *, BOOL_T,
                                  BOOL_T * );
STATUS_T BezierDescriptionMove(track_p trk,wAction_t action,coOrd pos );

