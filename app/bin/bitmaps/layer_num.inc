// Define digits for layer buttons

static char* n0_x16[] = {
	" #### "
	"######"
	"##  ##"
	"##  ##"
	"##  ##"
	"##  ##"
	"##  ##"
	"##  ##"
	"######"
	" #### "};

static char* n1_x16[] = {
	"  ##"
	" ###"
	"####"
	"  ##"
	"  ##"
	"  ##"
	"  ##"
	"  ##"
	"  ##"
	"  ##"};

static char* n2_x16[] = {
	" #### "
	"######"
	"##  ##"
	"    ##"
	"   ###"
	"  ### "
	" ###  "
	"###   "
	"######"
	"######"};

static char* n3_x16[] = {
	" #### "
	"######"
	"##  ##"
	"    ##"
	" #### "
	" #### "
	"    ##"
	"##  ##"
	"######"
	" #### "};

static char* n4_x16[] = {
	"    ##"
	"   ###"
	"  ####"
	" ## ##"
	"##  ##"
	"######"
	"######"
	"    ##"
	"    ##"
	"    ##"};

static char* n5_x16[] = {
	"######"
	"######"
	"##    "
	"##    "
	"####  "
	"  ####"
	"    ##"
	"##  ##"
	"######"
	" #### "};

static char* n6_x16[] = {
	" #### "
	"######"
	"##    "
	"##    "
	"##### "
	"######"
	"##  ##"
	"##  ##"
	"######"
	" #### "};

static char* n7_x16[] = {
	"######"
	"######"
	"    ##"
	"    ##"
	"   ## "
	"   ## "
	"   ## "
	"  ##  "
	"  ##  "
	"  ##  "};

static char* n8_x16[] = {
	" #### "
	"######"
	"##  ##"
	"##  ##"
	" #### "
	" #### "
	"##  ##"
	"##  ##"
	"######"
	" #### "};

static char* n9_x16[] = {
	" #### "
	"######"
	"##  ##"
	"##  ##"
	"######"
	" #####"
	"    ##"
	"    ##"
	"######"
	" #### "};

/* ***************** */

static char* n0_x24[] = {
	"  ######  "
	" ######## "
	"###    ###"
	"###    ###"
	"###    ###"
	"###    ###"
	"###    ###"
	"###    ###"
	"###    ###"
	"###    ###"
	"###    ###"
	"###    ###"
	"###    ###"
	" ######## "
	"  ######  "};

static char* n1_x24[] = {
	"  ###"
	" ####"
	"#####"
	"  ###"
	"  ###"
	"  ###"
	"  ###"
	"  ###"
	"  ###"
	"  ###"
	"  ###"
	"  ###"
	"  ###"
	"  ###"
	"  ###"};

static char* n2_x24[] = {
	"   ####   "
	" ######## "
	"####  ####"
	"###    ###"
	"       ###"
	"      ####"
	"     #### "
	"    ####  "
	"   ####   "
	"  ####    "
	" ####     "
	"####      "
	"###    ###"
	"##########"
	"##########"};

static char* n3_x24[] = {
	"   ####   "
	" ######## "
	"####  ####"
	"###    ###"
	"       ###"
	"      ####"
	"   ###### "
	"   #####  "
	"     #### "
	"      ####"
	"       ###"
	"###    ###"
	"####  ####"
	" ######## "
	"   ####   "};

static char* n4_x24[] = {
	"   #####  "
	"   #####  "
	"  ######  "
	"  ######  "
	" ### ###  "
	" ### ###  "
	" ### ###  "
	"###  ###  "
	"###  ###  "
	"##########"
	"##########"
	"     ###  "
	"     ###  "
	"     ###  "
	"     ###  "};

static char* n5_x24[] = {
	"##########"
	"##########"
	"###       "
	"###       "
	"###       "
	"###       "
	"########  "
	" ######## "
	"       ###"
	"       ###"
	"       ###"
	"       ###"
	"###    ###"
	"######### "
	" #######  "};

static char* n6_x24[] = {
	"   ####   "
	" ######## "
	"####  ####"
	"###    ###"
	"###       "
	"###       "
	"#######   "
	"######### "
	"###   ####"
	"###    ###"
	"###    ###"
	"###    ###"
	"####  ####"
	" ######## "
	"   ####   "};

static char* n7_x24[] = {
	"##########"
	"##########"
	"###    ###"
	"       ###"
	"      ### "
	"      ### "
	"     ###  "
	"     ###  "
	"     ###  "
	"    ###   "
	"    ###   "
	"    ###   "
	"   ###    "
	"   ###    "
	"   ###    "};

static char* n8_x24[] = {
	"   ####   "
	" ######## "
	"####  ####"
	"###    ###"
	"###    ###"
	"####  ####"
	" ######## "
	" ######## "
	"####  ####"
	"###    ###"
	"###    ###"
	"###    ###"
	"####  ####"
	" ######## "
	"   ####   "};

static char* n9_x24[] = {
	"   ####   "
	" ######## "
	"####  ####"
	"###    ###"
	"###    ###"
	"####   ###"
	" #########"
	"   #######"
	"       ###"
	"       ###"
	"       ###"
	"###    ###"
	"####  ####"
	" ######## "
	"   ####   "};

/* ***************** */

static char* n0_x32[] = {
	"    ######    "
	"  ##########  "
	" ############ "
	"#####    #####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"#####    #####"
	" ############ "
	"  ##########  "
	"    ######    "};

static char* n1_x32[] = {
	"  ####"
	" #####"
	"######"
	"######"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"
	"  ####"};

static char* n2_x32[] = {
	"    ######    "
	"  ##########  "
	" ############ "
	"#####    #####"
	"####      ####"
	"          ####"
	"          ####"
	"          ####"
	"        ##### "
	"       #####  "
	"      #####   "
	"     #####    "
	"    #####     "
	"   #####      "
	"  #####       "
	" #####        "
	"#####     ####"
	"##############"
	"##############"
	"##############"};

static char* n3_x32[] = {
	"    ######    "
	"  ##########  "
	" ############ "
	"#####    #####"
	"####      ####"
	"          ####"
	"          ####"
	"         #### "
	"     #######  "
	"     ######   "
	"     #######  "
	"         #### "
	"          ####"
	"          ####"
	"          ####"
	"####      ####"
	"#####    #####"
	" ############ "
	"  ##########  "
	"    ######    "};

static char* n4_x32[] = {
	"     #######  "
	"     #######  "
	"    ########  "
	"    ########  "
	"   #### ####  "
	"   #### ####  "
	"  ####  ####  "
	"  ####  ####  "
	" ####   ####  "
	" ####   ####  "
	"####    ####  "
	"####    ####  "
	"##############"
	"##############"
	"##############"
	"        ####  "
	"        ####  "
	"        ####  "
	"        ####  "
	"        ####  "};

static char* n5_x32[] = {
	"##############"
	"##############"
	"##############"
	"####          "
	"####          "
	"####          "
	"####          "
	"####          "
	"##########    "
	"############  "
	"############# "
	"         #####"
	"          ####"
	"          ####"
	"          ####"
	"####      ####"
	"#####    #####"
	" ############ "
	"  ##########  "
	"    ######    "};

static char* n6_x32[] = {
	"    ######    "
	"  ##########  "
	" ############ "
	"#####    #####"
	"####      ####"
	"####          "
	"####          "
	"####          "
	"##########    "
	"############  "
	"############# "
	"####     #####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"#####    #####"
	" ############ "
	"  ##########  "
	"    ######    "};

static char* n7_x32[] = {
	"##############"
	"##############"
	"##############"
	"####      ####"
	"####     #### "
	"         #### "
	"        ####  "
	"        ####  "
	"       ####   "
	"       ####   "
	"       ####   "
	"      ####    "
	"      ####    "
	"      ####    "
	"     ####     "
	"     ####     "
	"     ####     "
	"    ####      "
	"    ####      "
	"    ####      "};

static char* n8_x32[] = {
	"    ######    "
	"  ##########  "
	" ############ "
	"#####    #####"
	"####      ####"
	"####      ####"
	"####      ####"
	" ####    #### "
	"  ##########  "
	"   ########   "
	"  ##########  "
	" ####    #### "
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"#####    #####"
	" ############ "
	"  ##########  "
	"    ######    "};

static char* n9_x32[] = {
	"    ######    "
	"  ##########  "
	" ############ "
	"#####    #####"
	"####      ####"
	"####      ####"
	"####      ####"
	"####      ####"
	"#####     ####"
	" #############"
	"  ############"
	"    ##########"
	"          ####"
	"          ####"
	"          ####"
	"####      ####"
	"#####    #####"
	" ############ "
	"  ##########  "
	"    ######    "};
