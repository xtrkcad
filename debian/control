Source: xtrkcad
Section: editors
Priority: optional
Maintainer: Jörg Frings-Fürst <debian@jff.email>
Uploaders:
 Daniel E. Markle <dmarkle@ashtech.net>,
 Mike Gabriel <sunweaver@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 inkscape,
 libfreeimage-dev,
 libgtk2.0-dev,
 libzip-dev,
 pandoc
Rules-Requires-Root: no
Standards-Version: 4.7.0.0
Homepage: http://xtrkcad.org/
Vcs-Git: git://git.jff.email/xtrkcad.git
Vcs-Browser: https://git.jff.email/cgit/xtrkcad.git

Package: xtrkcad
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 xtrkcad-common (= ${source:Version})
Description: CAD program for designing model railroad layouts
 With XTrackCAD you can:
  Design layouts in any scale and gauge
  Use the predefined libraries for many popular brands of turnouts to
  help you get started easily
  Add your own favorite components
  Manipulate track much like you would with actual flex-track to modify
  extend and join tracks and turnouts
  Test your design by running trains, including picking them up and moving
  them with the mouse.
  At any point you can print the design in a scale of your choice. When
  printed in 1:1 scale the printout can be used as a template for laying
  the track to build your dream layout.
  Learning XTrackCAD is made easy with the extensive on-line help and
  demonstrations.

Package: xtrkcad-common
Architecture: all
Depends:
 ${misc:Depends}
Breaks:
 xtrkcad (<< 1:4.2.4a-1)
Replaces:
 xtrkcad (<< 1:4.2.4a-1)
Description: CAD program for designing model railroad layouts (common files)
 With XTrackCAD you can:
  Design layouts in any scale and gauge
  Use the predefined libraries for many popular brands of turnouts to
  help you get started easily
  Add your own favorite components
  Manipulate track much like you would with actual flex-track to modify
  extend and join tracks and turnouts
  Test your design by running trains, including picking them up and moving
  them with the mouse.
  At any point you can print the design in a scale of your choice. When
  printed in 1:1 scale the printout can be used as a template for laying
  the track to build your dream layout.
  Learning XTrackCAD is made easy with the extensive on-line help and
  demonstrations.
 .
 This package contains the demo, examples, parameter and the help files.
